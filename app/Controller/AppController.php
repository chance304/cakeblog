<?php

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

	public $components = array(
							'Flash',
							'Session',
							'Auth' => array(
								'loginRedirect' => array(
									'controller' => 'posts',
									'action' => 'index'
								),
								'logoutRedirect' => array(
									'controller' => 'pages',
									'action' => 'display','welcome'
								),
								'authenticate' => array(
									'Form' => array(
									'username'=>'username',
									'password'=>'password',	
									'passwordHasher' => 'Blowfish'
									)
								)
							)
						);

	public function beforeFilter() {
		$this->Auth->allow('login','display');
}

}
