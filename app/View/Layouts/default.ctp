<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		// echo $this->Html->css('cake.generic');

		echo $this->Html->css('bootstrap') ;
		echo $this->Html->script('jquery-3.2.1') ;
		echo $this->Html->script('bootstrap') ;

	?>
</head>
<body>
	<div id="container">
		<div id="header">
			<!-- <?php echo $this->fetch('navigation');?> -->
	    <nav class="navbar navbar-inverse">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="/">DeemaLab</a>
	        </div>

	        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	          <ul class="nav navbar-nav">
	            <li class><a href="/register">Application Registration<span class="sr-only">(current)</span></a></li>
	            <li><a href="/login">Login</a></li>
	          </ul>
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="#"></a></li>
	          </ul>
	        </div>
	      </div>
	    </nav>
		<div class="container">

			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
				<center>Copyright Shobhit Tripathi</center>
	<!-- 		<p>
				<?php echo $cakeVersion; ?>
			</p>
	 -->	</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
